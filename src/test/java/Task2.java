import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        final int attempts = 39;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter possible number of guests: ");
        System.out.println("Number should be > 2");
        int n = sc.nextInt();
        int count = 0;
        int peopleHeardRumor = 0;
        for (int i = 0; i < attempts; i++) {
            boolean guests[] = new boolean[n];
            guests[1] = true;
            boolean alreadyHeard = false;
            int nextPerson = -1;
            int currentPerson = 1;
            while (!alreadyHeard) {
                nextPerson = 1 + (int) (Math.random() * (n - 1));
                if (nextPerson == currentPerson) {
                    while (nextPerson == currentPerson)
                        nextPerson = 1 + (int) (Math.random() * (n - 1));
                }
                if (guests[nextPerson]) {
                    if (rumorSpreaded(guests))
                        count++;
                    peopleHeardRumor = peopleHeardRumor + countPeopleReached(guests);
                    alreadyHeard = true;
                }
                guests[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }
        System.out.println("Probability that everyone will hear rumor except Alice in " + attempts + " attempts: " +
                (double) count / attempts);
        System.out.println("Average amount of people that rumor reached is: " + peopleHeardRumor / attempts);
    }

    public static int countPeopleReached(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }

    public static boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }


}